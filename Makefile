JAVAC=javac
OUTDIR=out
all: src/eus/juaristi/*.java
ifneq ($(wildcard $(OUTDIR)/),)
	$(error Directory $(OUTDIR)/ already exists. Please delete it, as this is where output will be placed ('make clean' will do this for you).)
endif
	@mkdir $(OUTDIR)
	@$(JAVAC) -d $(OUTDIR)/ $^
	@echo "Done. You can now execute the program with: java -classpath $(OUTDIR) eus.juaristi.Main"
clean:
	@rm -rf ./$(OUTDIR)
