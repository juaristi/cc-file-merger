package eus.juaristi;

import java.io.*;
import java.util.*;

public class SequentialPagedUserReader extends PagedUserReader implements Iterator<User> {
    private File file;
    private int index;
    private ArrayList<User> users;
    private BufferedReader reader;

    /* Number of lines in a page */
    private static int pageLength = 20;

    public SequentialPagedUserReader(File file) {
        super(pageLength);
        this.file = file;
        index = 0;
        users = new ArrayList<>(pageLength);
    }

    public void start() throws FileNotFoundException {
        reader = new BufferedReader(new FileReader(file));
    }

    public void end() throws IOException {
        reader.close();
    }

    @Override
    public boolean hasNext() {
        advanceToNextPageIfNeeded();
        return users.size() != 0 && users.get(index) != null;
    }

    @Override
    public User next() {
        advanceToNextPageIfNeeded();

        if (users.size() == 0)
            throw new NoSuchElementException();

        return users.get(index++);
    }

    @Override
    User createUser(String str) {
        Scanner s = new Scanner(str);
        String id = s.next();
        StringBuffer name = new StringBuffer(s.next());

        /* Keep on reading the name until end of line */
        while (s.hasNext())
            name.append(" ").append(s.next());

        return new User(id, name.toString());
    }

    private void advanceToNextPageIfNeeded() {
        if (index == users.size()) {
            index = 0;
            users.clear();
            nextPage();
        }
    }

    private void nextPage() {
        if (reader == null)
            throw new IllegalStateException("Illegal state: did you call start()?");

        try {
            readNextPage(reader,
                    user -> users.add(user));
        } catch (NoSuchElementException e) {
            throw new FileFormatException(file.getName(), getLineNumber());
        } catch (IOException e) {
            throw new RuntimeException("IOException: Could not read file '" + file.getName() + "'");
        }
    }
}
