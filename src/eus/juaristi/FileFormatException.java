package eus.juaristi;

public class FileFormatException extends RuntimeException {
    public FileFormatException(String fileName, int lineNumber) {
        super("Syntax error in file '" + fileName + "': line " + lineNumber);
    }
}
