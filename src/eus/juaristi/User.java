package eus.juaristi;

public class User {
    private String id;
    private long phone;
    private String name;

    public User(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public User(String id, long phone) {
        this.id = id;
        this.phone = phone;
    }

    public User setName(String name) {
        this.name = name;
        return this;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public long getPhone() {
        return phone;
    }
}
