package eus.juaristi;

import java.io.*;
import java.util.function.Function;

public class FileMerger {
    private File left, right;
    private BufferedWriter os;

    public FileMerger(File left, File right) {
        this.left = left;
        this.right = right;
    }

    public void setOutputStream(OutputStream os) {
        this.os = new BufferedWriter(
                new OutputStreamWriter(os));
    }

    public void merge() throws IOException {
        if (os == null)
            throw new IllegalStateException("No output stream. Did you call setOutputStream()?");

        /*
         * We've got two files here: left and right.
         * Both files are read in pages to avoid bringing them entirely to memory,
         * but the paging strategy is different.
         *
         * In the left file, records are processed sequentially until the current page has been fully processed.
         * Then the current page is discarded and the next one is read.
         * Records are read and kept in pre-allocated ArrayList for fast sequential access + insertion.
         *
         * In the right file, pages are read as needed since records are accessed randomly and the record we're
         * looking for might be anywhere in the file. However once a record is used it is discarded to liberate memory.
         * Records are kept in a red-black tree (a TreeMap in Java) for efficient query + deletion.
         *
         * Currently, the left file is the names file. Right file is the phones file.
         * In theory, they could be used interchangeably. But since the syntax is different (phones are numbers
         * while names are strings) we need to hard-wire the left and right files to avoid getting too complex.
         * Making these classes generic would require refactoring the internal nextPage() and createUser() methods,
         * and that would be too much hassle for such a simple program.
         */
        SequentialPagedUserReader leftReader = new SequentialPagedUserReader(left);
        RandomPagedUserReader rightReader = new RandomPagedUserReader(right);

        doMerge(leftReader, u -> {
            User user = rightReader.pollUser(u);
            if (user == null)
                return null;
            return user.setName(u.getName());
        });
    }

    private void doMerge(SequentialPagedUserReader userReader, Function<User, User> fn) throws IOException {
        userReader.start();

        try {
            while (userReader.hasNext()) {
                User user = fn.apply(userReader.next());
                if (user != null)
                    writeUser(user);
            }
        } finally {
            userReader.end();
        }
    }

    private void writeUser(User user) throws IOException {
        String str = user.getId()
                + "    " + user.getName()
                + "    " + user.getPhone();
        os.write(str);
        os.newLine();
        os.flush();
    }
}
