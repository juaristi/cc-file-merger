package eus.juaristi;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        FileMerger fm;
        String namesFilename = null,
                phonesFilename = null,
                outputFilename = null;

        if (args.length == 0) {
            System.out.println("************************************************");
            System.out.println("*                C/C File Merger               *");
            System.out.println("*                                              *");
            System.out.println("*                            by Ander Juaristi *");
            System.out.println("************************************************");

            Scanner s = new Scanner(System.in);

            System.out.print("Names file: ");
            namesFilename = s.nextLine().trim();

            System.out.print("Phones file: ");
            phonesFilename = s.nextLine().trim();

            System.out.print("Output file ('-' or empty for stdout): ");
            outputFilename = s.nextLine().trim();
        } else if (args.length == 3) {
            namesFilename = args[0].trim();
            phonesFilename = args[1].trim();
            outputFilename = args[2].trim();
        } else {
            System.err.println("Invalid number of arguments (either 3, or zero). Exiting.");
            System.exit(1);
        }

        try {
            File namesFile = new File(namesFilename),
                    phonesFile = new File(phonesFilename);

            if (namesFile.equals(phonesFile)) {
                System.err.println("ERROR: Input files are both the same file.");
                System.exit(1);
            }

            fm = new FileMerger(namesFile, phonesFile);

            if (outputFilename.equals("-") || outputFilename.equals(""))
                fm.setOutputStream(System.out);
            else
                fm.setOutputStream(new FileOutputStream(new File(outputFilename)));

            fm.merge();
        } catch (FileFormatException e) {
            System.err.println(e.getMessage());
            System.exit(1);
        } catch (IOException e) {
            System.err.println("ERROR: " + e.getMessage());
            System.exit(1);
        }
    }
}
