package eus.juaristi;

import java.io.*;
import java.util.*;

public class RandomPagedUserReader extends PagedUserReader {
    private TreeMap<String, User> users;
    private String fileName;
    private BufferedReader reader;
    private boolean readerFinished;

    private static int pageLength = 20;

    public RandomPagedUserReader(File file) throws IOException {
        super(pageLength);
        users = new TreeMap<>();
        fileName = file.getName();
        readerFinished = false;
        reader = new BufferedReader(new FileReader(file));
    }

    public User pollUser(User user) {
        return pollUser(user.getId());
    }

    public User pollUser(String id) {
        try {
            for (;;) {
                /* remove() returns null if not found */
                User user = users.remove(id);
                if (user == null)
                    nextPage();
                else
                    return user;
            }
        } catch (NoSuchElementException | IOException e) {
            /* We reached the end of the file and the user was not found */
            return null;
        }
    }

    @Override
    User createUser(String str) {
        Scanner s = new Scanner(str);
        String id = s.next();
        StringBuffer phone = new StringBuffer(s.next());

        /* Handle country code */
        if (phone.charAt(0) == '+')
            phone.replace(0, 1, "00");

        /* Keep on reading phone until end of line (might have spaces) */
        while (s.hasNext())
            phone.append(s.next());

        return new User(id, Long.parseUnsignedLong(phone.toString()));
    }

    private void nextPage() throws IOException {
        if (readerFinished)
            throw new NoSuchElementException();

        try {
            readNextPage(reader,
                    user -> users.put(user.getId(), user),
                    () -> readerFinished = true);
        } catch (NoSuchElementException | NumberFormatException e) {
            /* These two are thrown by createUser() when the record (line) format is incorrect */
            throw new FileFormatException(fileName, getLineNumber());
        } catch (IOException e) {
            /*
             * IOExceptions in files are always very likely caused by some inconsistency in some layer beneath
             * we're very unlikely to recover from. It's better to just terminate everything here.
             */
            readerFinished = true;
            throw e;
        } finally {
            if (readerFinished)
                reader.close();
        }
    }
}
