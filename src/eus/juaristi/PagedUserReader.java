package eus.juaristi;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.function.Consumer;

public abstract class PagedUserReader {
    private int pageLength, lineNumber;

    PagedUserReader(int pageLength) {
        this.pageLength = pageLength;
        lineNumber = 0;
    }

    abstract User createUser(String line);

    int getLineNumber() {
        return lineNumber;
    }

    final void readNextPage(BufferedReader reader, Consumer<User> consumer) throws IOException {
        readNextPage(reader, consumer, null);
    }

    final void readNextPage(BufferedReader reader,
                      Consumer<User> consumer, Runnable finalizer) throws IOException {
        User user;
        String line;
        int linesRead = 0;

        while (linesRead < pageLength) {
            line = reader.readLine();

            if (line == null) {
                if (finalizer != null)
                    finalizer.run();
                break;
            }

            lineNumber++;

            if (line.trim().isEmpty())
                continue;

            user = createUser(line);
            //users.put(user.getId(), user);
            consumer.accept(user);

            linesRead++;
        }
    }
}
