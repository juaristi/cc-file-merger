# Usage 

There is a Makefile included that compiles the source code and puts it into
a directory named `out`. If this directory already exists, it fails.

This is written in Java 8 and as such needs a compatible `javac` and JVM. You would
usually get away by installing `openjdk-8-jdk` in Debian derivatives.

Other than taking into account the above paragraphs, there's nothing else to do.
This is the easiest to execute software in the world:

```
make && java -classpath out eus.juaristi.Main
```

# Remarks

The classes `RandomPagedUserReader` and `SequentialPagedUserReader` could yet
be refactored a bit. In particular, there's some ugly code duplication in the
`createUser()` and `nextPage()` methods. These could be placed in a superclass
and make those classes generic.

However that would convolute such a simple program, and I didn't want to do it.
UPDATE: I finally did it ;)

